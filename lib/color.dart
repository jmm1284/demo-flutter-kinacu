import 'package:flutter/material.dart';

class ThemeColor {
  ThemeColor();

  static Color primary(int value) {
    return _primaryColorMap[value];
  }

  static Color accent(int value) {
    return _accentColorMap[value];
  }


  //generated with http://mcg.mbitson.com/ from #3DCCCA
  static Map<int, Color> _primaryColorMap = {
    50: Color(0xFFE8F9F9),
    100: Color(0xFFC5F0EF),
    200: Color(0xFF9EE6E5),
    300: Color(0xFF77DBDA),
    400: Color(0xFF5AD4D2),
    500: Color(0xFF3DCCCA), //standard (0CAABB)
    600: Color(0xFF37C7C5),
    700: Color(0xFF2FC0BD),
    800: Color(0xFF27B9B7),
    900: Color(0xFF1AADAB),
  };

  static Map<int, Color> _accentColorMap = {
    50: Color(0xFFFEFBEC),
    100: Color(0xFFFEF5D1),
    200: Color(0xFFFDEEB2),
    300: Color(0xFFFCE793),
    400: Color(0xFFFBE27B),
    500: Color(0xFFFADD64),
    600: Color(0xFFF9D95C),
    700: Color(0xFFF9D452),
    800: Color(0xFFF8CF48),
    900: Color(0xFFF6C736),
  };

}
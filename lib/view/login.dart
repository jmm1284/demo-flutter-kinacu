import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import '../i18n.dart';
import '../color.dart';

class LoginView extends StatefulWidget {
  @override
  _LoginViewState createState() => _LoginViewState();
}

class _LoginViewState extends State<LoginView> {
  bool _passwordHidden;

  @override
  void initState() {
    _passwordHidden = true;
  }

  @override
  Widget build(BuildContext context) {
    double screenWidth = MediaQuery.of(context).size.width;

    return new Scaffold(
      body: new SafeArea(
        child: new GestureDetector(
          onTap: () {
            FocusScopeNode currentFocus = FocusScope.of(context);
            if (!currentFocus.hasPrimaryFocus) {
              currentFocus.unfocus();
            }
          },
          child: new Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              new Expanded(
                child: new Container(
                  height: MediaQuery.of(context).size.width,
                  decoration: new BoxDecoration(
                    color: Theme.of(context).primaryColor,
                    borderRadius: new BorderRadius.only(
                      bottomLeft: Radius.elliptical(screenWidth * 4, 100),
                      bottomRight: Radius.elliptical(screenWidth * 4, 100)
                    )
                  ),
                ),
                flex: 2,
              ),
              new SizedBox(
                height: 32.0,
              ),
              new Padding(
                padding: const EdgeInsets.symmetric(horizontal: 16.0),
                child: new Column(
                  children: <Widget>[
                    new Padding(
                      padding: const EdgeInsets.symmetric(vertical: 4.0),
                      child: new TextField(
                        keyboardType: TextInputType.emailAddress,
                        decoration: CustomInputDecoration()
                          .withLabel(I18n.of(context).getValueOf(Strings.INPUT_LABEL_EMAIL))
                          .inputDecoration
                      ),
                    ),

                    new Padding(
                      padding: const EdgeInsets.symmetric(vertical: 4.0),
                      child: new TextField(
                        keyboardType: TextInputType.text,
                        obscureText: _passwordHidden,
                        decoration: CustomInputDecoration()
                          .withLabel(I18n.of(context).getValueOf(Strings.INPUT_LABEL_PASSWORD))
                          .withSuffix(
                            new IconButton(
                              icon: new Icon(_passwordHidden
                                ? Icons.visibility
                                : Icons.visibility_off),
                              onPressed: () {
                                setState(() {
                                  _passwordHidden = !_passwordHidden;
                                });
                              }
                            )
                          )
                          .inputDecoration
                      ),
                    ),

                    new Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        new FlatButton(
                          onPressed: () {
                            showDialog(
                              context: context,
                              builder: (BuildContext buildContext) {
                                return new ResetPasswordDialog();
                              }
                            );
                          },
                          child: new Text(I18n.of(context).getValueOf(Strings.LOGIN_FORGOT_PASSWORD)),
                        ),
                        new RaisedButton(
                          onPressed: (){
                            //TODO: check credentials
                            Navigator.pushNamed(context, '/home');
                          },
                          child: new Text(I18n.of(context).getValueOf(Strings.LOGIN_ACTION)),
                          color: Theme.of(context).accentColor,
                        ),
                      ],
                    ),

                    new SizedBox(
                      height: 16.0,
                    ),

                    new Row(
                      children: <Widget>[
                        new Expanded(child: new Divider()),
                        new Text(I18n.of(context).getValueOf(Strings.OR_SIGN_IN_WITH)),
                        new Expanded(child: new Divider()),
                      ],
                    ),

                    new SizedBox(
                      height: 16.0,
                    ),

                    new Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: <Widget>[

                        new SignInButton(
                          color: Colors.blueAccent,
                          icon: FontAwesomeIcons.facebookF,
                          onPressed: () {
                            //TODO: implement facebook login
                            Navigator.pushNamed(context, '/home');
                          }
                        ),

                        new SignInButton(
                          color: Colors.red,
                          icon: FontAwesomeIcons.google,
                          onPressed: () {
                            //TODO: implement google login
                            Navigator.pushNamed(context, '/home');
                          }
                        ),

                      ],
                    ),

                  ],
                ),
              ),

              new Spacer(
                flex: 1,
              ),

              new Padding(
                padding: const EdgeInsets.symmetric(horizontal: 8.0),
                child: new FlatButton(
                  onPressed: () {
                    Navigator.pushNamed(context, '/register');
                  },
                  child: new Text(I18n.of(context).getValueOf(Strings.LOGIN_REGISTER_BUTTON))
                )
              )
            ],
          )
        )
      )
    );
  }

}

class ResetPasswordDialog extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new AlertDialog(
      title: new Text(I18n.of(context).getValueOf(Strings.RESET_PWD_TITLE)),
      content: new Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          new Text(I18n.of(context).getValueOf(Strings.RESET_PWD_CONTENT)),
          new Container(
            padding: const EdgeInsets.only(top: 16.0),
            child: new TextField(
                keyboardType: TextInputType.emailAddress,
                decoration: CustomInputDecoration()
                    .withLabel(I18n.of(context).getValueOf(Strings.INPUT_LABEL_EMAIL))
                    .inputDecoration
            ),
          )
        ],
      ),
      actions: <Widget>[
        new FlatButton(
          textColor: ThemeColor.accent(900),
          onPressed: () {
            Navigator.of(context).pop();
          },
          child: new Text(I18n.of(context).getValueOf(Strings.BUTTON_CANCEL))
        ),
        new FlatButton(
          textColor: ThemeColor.accent(900),
          onPressed: () {
            //TODO: implement mail sending
          },
          child: new Text(I18n.of(context).getValueOf(Strings.BUTTON_SEND))
        )
      ],
    );
  }
}


class SignInButton extends StatelessWidget {
  final Color color;
  final IconData icon;
  final VoidCallback onPressed;

  SignInButton({Key key, @required this.color, @required this.icon, @required this.onPressed}): super(key: key);

  @override
  Widget build(BuildContext context) {
    double _buttonSize = 56.0;
    double _iconSize = 24.0;

    return new Container(
      width: _buttonSize,
      height: _buttonSize,
      decoration: new BoxDecoration(
        color: color,
        shape: BoxShape.circle,
      ),
      child: new IconButton(
        iconSize: _iconSize,
        icon: new Icon(icon,
          color: Colors.white,
        ),
        onPressed: onPressed,
      ),
    );
  }
}

class CustomInputDecoration {
  InputDecoration defaultDecoration;

  CustomInputDecoration() {
    defaultDecoration =
    new InputDecoration(
      contentPadding: const EdgeInsets.symmetric(
        vertical: 12.0,
        horizontal: 8.0
      ),
      focusedBorder: OutlineInputBorder(
        borderSide: BorderSide(
          color: ThemeColor.primary(500),
          width: 2.0
        ),
        gapPadding: 4.0
      ),
      enabledBorder: OutlineInputBorder(
        borderSide: BorderSide(
          color: ThemeColor.primary(900),
          width: 1.0
        ),
        gapPadding: 4.0
      )
    );

  }

  InputDecoration get inputDecoration => defaultDecoration;

  CustomInputDecoration withLabel(String label) {
    defaultDecoration = defaultDecoration.copyWith(labelText: label);
    return this;
  }

  CustomInputDecoration withSuffix(Widget widget) {
    defaultDecoration = defaultDecoration.copyWith(suffixIcon: widget);
    return this;
  }

  CustomInputDecoration withError() {
    defaultDecoration = defaultDecoration.copyWith(
      focusedErrorBorder: OutlineInputBorder(
        borderSide: BorderSide(color: ThemeColor.primary(900), width: 1.0),
        gapPadding: 4.0
      ),
      errorBorder: OutlineInputBorder(
          borderSide: BorderSide(color: ThemeColor.primary(900), width: 1.0),
          gapPadding: 4.0
      ),
    );
    return this;
  }

}

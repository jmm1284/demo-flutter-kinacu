import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';

import '../booking.dart';

class DateTimePicker extends StatefulWidget {

  final BookingProviderState state;

  const DateTimePicker({Key key, this.state}) : super(key : key);

  @override
  _DateTimePickerState createState() => _DateTimePickerState();
}

class _DateTimePickerState extends State<DateTimePicker> {

  DateTime dateTime;

  @override
  void initState() {
    super.initState();
    dateTime = widget.state.bookingState.dateTime;
  }

  @override
  Widget build(BuildContext context) {


    DateTime now = DateTime.now();
    DateTime initialDateTime = DateTime(now.year, now.month, now.day, now.hour, (now.minute ~/ 15) * 15);

    return new Container(
      height: MediaQuery.of(context).copyWith().size.height / 3,
      child: new CupertinoDatePicker(
        onDateTimeChanged: (DateTime newDateTime) {
          setState(() {
            if (newDateTime.isBefore(DateTime.now())){
              widget.state.updateDateTime(null);
            } else {
              setState(() {
                widget.state.updateDateTime(newDateTime);
              });
            }
          });
        },
        initialDateTime: initialDateTime.add(Duration(hours: 2)),
        mode: CupertinoDatePickerMode.dateAndTime,
        use24hFormat: true,
        minimumDate: DateTime(now.year, now.month, now.day),
        maximumDate: initialDateTime.add(Duration(days: 30)),
        minuteInterval: 15,
      ),
    );
  }


}
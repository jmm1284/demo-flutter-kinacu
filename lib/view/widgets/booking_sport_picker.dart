import 'package:flutter/material.dart';

import '../booking.dart';

class SportPicker extends StatefulWidget {
  final BookingProviderState state;

  const SportPicker({Key key, this.state}) : super(key : key);

  @override
  _SportPickerState createState() => _SportPickerState();
}

class _SportPickerState extends State<SportPicker> {

  List<String> sports = ['Tenis', 'Padel', 'Futbol', 'asdfas', 'zxvcvz', 'qerqwer'];

  int sportId;
  String sportName;

  @override
  void initState() {
    super.initState();
    sportId = widget.state.bookingState.sportId;
    sportName = widget.state.bookingState.sportName;
  }

  @override
  Widget build(BuildContext context) {
    return new Container(
      height: MediaQuery.of(context).copyWith().size.height / 3,
      child: ListView.builder(
        itemCount: sports.length,
        itemBuilder: (BuildContext context, int index){
          return new Container(
            width: MediaQuery.of(context).copyWith().size.width,
            child: new Column(
              children: <Widget>[
                index != 0
                  ? new Padding(
                    padding: const EdgeInsets.only(right: 16, left: 16),
                    child: new Divider(
                      height: 0,
                      thickness: 1,
                    ),
                  )
                  : new Container(),
                new ListTile(
                  leading: Icon(Icons.check),
                  title: new Text(sports[index]),
                  onTap: () {
                    setState(() {
                      widget.state.updateSport(index, sports[index]);
                    });
                    Navigator.pop(context);
                  },
                )
              ],
            )
          );
        }
      ),
    );
  }
}
import 'package:flutter/material.dart';

import '../booking.dart';
import '../../i18n.dart';

class DurationPicker extends StatefulWidget {

  final BookingProviderState state;

  const DurationPicker({Key key, this.state}) : super(key : key);

  @override
  _DurationPickerState createState() => _DurationPickerState();
}

class _DurationPickerState extends State<DurationPicker> {

  int duration;

  @override
  void initState() {
    super.initState();
    duration = widget.state.bookingState.duration;
  }

  @override
  Widget build(BuildContext context) {
    final List<String> durations = [
      I18n.of(context).getValueOf(Strings.BOOKING_DURATION_60),
      I18n.of(context).getValueOf(Strings.BOOKING_DURATION_90),
      I18n.of(context).getValueOf(Strings.BOOKING_DURATION_120),
    ];

    return new Container(
      height: MediaQuery.of(context).copyWith().size.height / 4,
      child: ListView.builder(
        itemCount: durations.length,
        itemBuilder: (BuildContext context, int index){
          return new Container(
            width: MediaQuery.of(context).copyWith().size.width,
            child: new Column(
              children: <Widget>[
                index != 0
                  ? new Padding(
                    padding: const EdgeInsets.only(right: 16, left: 16),
                    child: new Divider(
                      height: 0,
                      thickness: 1,
                    ),
                  )
                  : new Container(),
                new ListTile(
                  title: new Text(durations[index]),
                  onTap: () {
                    setState(() {
                      widget.state.updateDuration(index * 30 + 60, durations[index]);
                    });
                    Navigator.pop(context);
                  },
                )
              ],
            )
          );
        }
      ),
    );
  }
}
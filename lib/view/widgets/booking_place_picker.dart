import 'package:flutter/material.dart';
import '../../i18n.dart';
import '../booking.dart';

class PlacePicker extends StatefulWidget {
  final BookingProviderState state;

  const PlacePicker({Key key, this.state}) : super(key : key);

  @override
  _PlacePickerState createState() => _PlacePickerState();
}

class _PlacePickerState extends State<PlacePicker> {
  int businessId;

  @override
  void initState() {
    super.initState();
    businessId = widget.state.bookingState.businessId;
  }

  @override
  Widget build(BuildContext context) {
    final List<String> places = [
      I18n.of(context).getValueOf(Strings.BOOKING_PLACE_CLOSE_TO_ME),
      'Poliesportiu municipal de Valldemossa',
      'Ajuntament de Puigpunyent'
    ];

    return new Container(
      height: MediaQuery.of(context).copyWith().size.height / 4,
      child: ListView.builder(
        itemCount: places.length,
        itemBuilder: (BuildContext context, int index){
          return new Container(
            width: MediaQuery.of(context).copyWith().size.width,
            child: new Column(
              children: <Widget>[
                index != 0
                  ? new Padding(
                    padding: const EdgeInsets.only(right: 16, left: 16),
                    child: new Divider(
                      height: 0,
                      thickness: 1,
                    ),
                  )
                  : new Container(),
                new ListTile(
                  leading: index == 0
                    ? new Icon(Icons.gps_fixed)
                    : new Icon(Icons.home),
                  title: new Text(places[index]),
                  onTap: () {
                    setState(() {
                      widget.state.updatePlace(index, places[index]);
                    });
                    Navigator.pop(context);
                  },
                )
              ],
            )
          );
        }
      ),
    );
  }
}
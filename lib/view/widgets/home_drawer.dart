import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import '../../color.dart';
import '../../i18n.dart';

class HomeDrawer extends StatefulWidget {
  @override
  _HomeDrawerState createState() => _HomeDrawerState();
}

class _HomeDrawerState extends State<HomeDrawer> {

  @override
  Widget build(BuildContext context) {
    double screenWidth = MediaQuery.of(context).size.width;

    return new Drawer(
      child: new ListView(
        children: <Widget>[
          new DrawerHeader(
            child: new Container(
              child: new CircleAvatar(
                backgroundColor: ThemeColor.accent(500),
                child: new ClipOval(
                  child: new Image.network('https://www.publicdomainpictures.net/pictures/270000/velka/avatar-people-person-business-u.jpg'),
                ),
              ),
            ),
            decoration: new BoxDecoration(
                color: ThemeColor.accent(500)
            ),
            margin: const EdgeInsets.only(bottom: 0),
          ),

          new Stack(
            children: <Widget>[
              new Container(
                height: 30,
                margin: const EdgeInsets.only(bottom: 10),
                decoration: new BoxDecoration(
                    color: ThemeColor.accent(500),
                    borderRadius: new BorderRadius.only(
                        bottomLeft: Radius.elliptical(screenWidth * 4, 100),
                        bottomRight: Radius.elliptical(screenWidth * 4, 100)
                    )
                ),
              ),

              new Align(
                alignment: Alignment.topCenter,
                child: new Padding(
                  padding: const EdgeInsets.only(top: 0.0),
                  child: new Text('Julián Martínez',
                    style: TextStyle(
                        fontSize: 16.0,
                        fontWeight: FontWeight.bold
                    ),
                  ),
                ),
              )
            ],
          ),


          new ListTile(
            title: new Text(I18n.of(context).getValueOf(Strings.DRAWER_EDIT_PROFILE)),
            leading: new Icon(Icons.edit),
            onTap: () {},
          ),
          new ListTile(
            title: new Text(I18n.of(context).getValueOf(Strings.DRAWER_INVITE_FRIEND)),
            leading: new Icon(Icons.person_add),
            onTap: () {},
          ),
          new ListTile(
            title: new Text(I18n.of(context).getValueOf(Strings.DRAWER_REPORT_ISSUE)),
            leading: new Icon(Icons.report_problem),
            onTap: () {},
          ),
          new ListTile(
            title: new Text(I18n.of(context).getValueOf(Strings.DRAWER_LOG_OUT)),
            leading: new Icon(FontAwesomeIcons.signOutAlt),
            onTap: () {
              Navigator.popUntil(context, ModalRoute.withName('/'));
            },
          ),
        ],
      ),
    );
  }
}

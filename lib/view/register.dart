import 'package:flutter/material.dart';
import 'login.dart';

import '../i18n.dart';


/*
TODO: validador de contraseñas
TODO: input para birthdate
 */
class RegisterView extends StatefulWidget {
  @override
  _RegisterViewState createState() => _RegisterViewState();
}

class _RegisterViewState extends State<RegisterView> {
  final _formKey = GlobalKey<FormState>();
  bool _passwordHidden;
  bool _confirmHidden;

  @override
  void initState() {
    _passwordHidden = true;
    _confirmHidden = true;
  }

  @override
  Widget build(BuildContext context) {

    final RegExp emailRegex = RegExp(r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+");
    double screenWidth = MediaQuery.of(context).size.width;

    return new Scaffold(
      appBar: new AppBar(
        title: new Text(I18n.of(context).getValueOf(Strings.REGISTER_TITLE)),
        elevation: 0,
      ),
      body: new SafeArea(
        child: new Stack(
          children: <Widget>[
            new Container(
              height: 16,
              decoration: new BoxDecoration(
                color: Theme.of(context).primaryColor,
                borderRadius: new BorderRadius.only(
                  bottomLeft: Radius.elliptical(screenWidth * 4, 100),
                  bottomRight: Radius.elliptical(screenWidth * 4, 100)
                )
              ),
            ),

            new GestureDetector(
              onTap: () {
                FocusScopeNode currentFocus = FocusScope.of(context);
                if (!currentFocus.hasPrimaryFocus) {
                  currentFocus.unfocus();
                }
              },
              child: new Padding(
                padding: EdgeInsets.only(top: 16.0, bottom: 4.0),
                child: new Form(
                  key: _formKey,
                  child: new Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 8.0),
                    child: new Column(
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: <Widget>[
                        new Expanded(
                          child: new SingleChildScrollView(
                            child: new Padding(
                              padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 8.0),
                              child: new Column(
                                children: <Widget>[

                                  new CustomTextFormField(
                                    label: I18n.of(context).getValueOf(Strings.INPUT_LABEL_EMAIL),
                                    textInputType: TextInputType.emailAddress,
                                    validator: (value) => !emailRegex.hasMatch(value)
                                        ? I18n.of(context).getValueOf(Strings.VALIDATE_EMAIL)
                                        : null,
                                  ),

                                  new CustomTextFormField(
                                    label: I18n.of(context).getValueOf(Strings.INPUT_LABEL_PASSWORD),
                                    obscureText: _passwordHidden,
                                    textInputType: TextInputType.text,
                                    suffix: new IconButton(
                                      icon: new Icon(_passwordHidden
                                          ? Icons.visibility
                                          : Icons.visibility_off),
                                      onPressed: () {
                                        setState(() {
                                          _passwordHidden = !_passwordHidden;
                                        });
                                      }
                                    )

                                  ),
                                  new CustomTextFormField(
                                    label: I18n.of(context).getValueOf(Strings.INPUT_LABEL_REPEAT_PASSWORD),
                                    obscureText: _confirmHidden,
                                    textInputType: TextInputType.text,
                                    suffix: new IconButton(
                                      icon: new Icon(_confirmHidden
                                          ? Icons.visibility
                                          : Icons.visibility_off),
                                      onPressed: () {
                                        setState(() {
                                          _confirmHidden = !_confirmHidden;
                                        });
                                      }
                                    )
                                  ),

                                  new CustomTextFormField(
                                    label: I18n.of(context).getValueOf(Strings.INPUT_LABEL_NAME),
                                    textInputType: TextInputType.text,
                                    validator: (value) => value.isEmpty ? I18n.of(context).getValueOf(Strings.VALIDATE_FIELD) : null,
                                  ),

                                  new CustomTextFormField(
                                    label: I18n.of(context).getValueOf(Strings.INPUT_LABEL_SURNAME),
                                    textInputType: TextInputType.text,
                                    validator: (value) => value.isEmpty ? I18n.of(context).getValueOf(Strings.VALIDATE_FIELD) : null,
                                  ),

                                  new CustomTextFormField(
                                    label: I18n.of(context).getValueOf(Strings.INPUT_LABEL_BIRTHDATE),
                                    textInputType: TextInputType.text,
                                    validator: (value) => value.isEmpty ? I18n.of(context).getValueOf(Strings.VALIDATE_FIELD) : null
                                  ),

                                  new CustomTextFormField(
                                    label: I18n.of(context).getValueOf(Strings.INPUT_LABEL_PHONE),
                                    textInputType: TextInputType.text,
                                  ),

                                  new DropdownButtonHideUnderline(
                                    child: new Padding(
                                      padding: const EdgeInsets.symmetric(horizontal: 0.0, vertical: 4.0),
                                      child: new InputDecorator(
                                        decoration: InputDecoration(
                                          labelText: I18n.of(context).getValueOf(Strings.GENDER_SELECTOR),
                                          contentPadding: EdgeInsets.zero,
                                        ),
                                        isEmpty: false,
                                        child: DropdownButton<String>(
                                          value: '3',
                                          onChanged: (String newValue) {
                                            /*setState(() {
                                              oldValue = newValue;
                                            });*/
                                          },
                                          items: [
                                            new DropdownMenuItem(
                                              value: '3',
                                              child: new Text(I18n.of(context).getValueOf(Strings.GENDER_NOT_SPECIFIED)),
                                            ),
                                            new DropdownMenuItem(
                                              value: '1',
                                              child: new Text(I18n.of(context).getValueOf(Strings.GENDER_MALE)),
                                            ),
                                            new DropdownMenuItem(
                                              value: '2',
                                              child: new Text(I18n.of(context).getValueOf(Strings.GENDER_FEMALE)),
                                            )
                                          ],
                                        ),
                                      ),
                                    )
                                  ),


                                ],
                              )
                            )
                          )
                        ),

                        new RaisedButton(
                          disabledColor: Colors.grey,
                          color: Theme.of(context).accentColor,
                          onPressed: () {
                            if (_formKey.currentState.validate()) {
                              /*
                              Scaffold.of(context).showSnackBar(
                                new SnackBar(
                                  content: new Text(I18n.of(context).getValueOf(Strings.REGISTER_SNACK))
                                )
                              );
                              */
                              Navigator.pushNamed(context, '/home');
                            }
                          },
                          child: new Text(I18n.of(context).getValueOf(Strings.REGISTER_BUTTON))
                        )
                      ],
                    )
                  )
                )
              )
            )
          ],
        )

      ),
    );
  }
}



class CustomTextFormField extends StatelessWidget {

  final String label;
  final Widget suffix;
  final TextInputType textInputType;
  final bool obscureText;
  final FormFieldValidator<String> validator;

  CustomTextFormField({
    Key key,
    @required this.label,
    @required this.textInputType,
    this.suffix,
    this.obscureText = false,
    this.validator
  }) : super (key: key);

  @override
  Widget build(BuildContext context) {
    return new Padding(
      padding: const EdgeInsets.symmetric(vertical: 4.0),
      child: new TextFormField(
        keyboardType: textInputType,
        obscureText: obscureText,
        decoration: CustomInputDecoration()
            .withLabel(label)
            .withError()
            .withSuffix(suffix != null ? suffix : new SizedBox())
            .inputDecoration,
        //textInputAction: TextInputAction.next,
        validator: validator
      ),
    );
  }
}


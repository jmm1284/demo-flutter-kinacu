import 'package:flutter/material.dart';
import '../i18n.dart';

class CheckoutView extends StatefulWidget {
  @override
  _CheckoutViewState createState() => _CheckoutViewState();
}

class _CheckoutViewState extends State<CheckoutView> {

  @override
  Widget build(BuildContext context) {
    double screenWidth = MediaQuery.of(context).size.width;
    final List<String> _allPaymentMethods = <String>[
      I18n.of(context).getValueOf(Strings.CHECKOUT_PAYMENT_CARD),
      I18n.of(context).getValueOf(Strings.CHECKOUT_PAYMENT_CREDIT) + ' ' + '(39,20€)',
      I18n.of(context).getValueOf(Strings.CHECKOUT_PAYMENT_IN_PERSON)
    ];

    return new Scaffold(
      appBar: new AppBar(
        title: new Text(I18n.of(context).getValueOf(Strings.CHECKOUT_TITLE)),
        elevation: 0,
      ),
      body: new SafeArea(
        child: new Stack(
          children: <Widget>[
            new Container(
              height: 30,
              decoration: new BoxDecoration(
                color: Theme.of(context).primaryColor,
                borderRadius: new BorderRadius.only(
                    bottomLeft: Radius.elliptical(screenWidth * 4, 100),
                    bottomRight: Radius.elliptical(screenWidth * 4, 100)
                )
              ),
            ),

            new Padding(
              padding: EdgeInsets.only(top: 36.0, left: 16.0, right: 16.0, bottom: 4.0),
              child: new Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: <Widget>[
                  new CheckoutDataRow(
                      field: I18n.of(context).getValueOf(Strings.CHECKOUT_FIELD_SPORT),
                      data: 'Tenis (superficie)'
                  ),
                  new CheckoutDataRow(
                      field: I18n.of(context).getValueOf(Strings.CHECKOUT_FIELD_CLUB),
                      data: 'Club 1'
                  ),
                  new CheckoutDataRow(
                      field: I18n.of(context).getValueOf(Strings.CHECKOUT_FIELD_COURT),
                      data: 'Tenis 1'
                  ),
                  new CheckoutDataRow(
                      field: I18n.of(context).getValueOf(Strings.CHECKOUT_FIELD_EXTRAS),
                      data: 'Ninguno'
                  ),
                  new CheckoutDataRow(
                      field: I18n.of(context).getValueOf(Strings.CHECKOUT_FIELD_DATE),
                      data: 'Domingo 24/11/19'
                  ),
                  new CheckoutDataRow(
                      field: I18n.of(context).getValueOf(Strings.CHECKOUT_FIELD_TIME),
                      data: '18:00 - 19:00'
                  ),
                  new CheckoutDataRow(
                      field: I18n.of(context).getValueOf(Strings.CHECKOUT_FIELD_PRICE),
                      data: '6,00€'
                  ),

                  new SizedBox(height: 40,),
                  new DropdownButtonHideUnderline(
                    child: new Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 0.0),
                      child: new InputDecorator(
                        decoration: InputDecoration(
                          labelText: I18n.of(context).getValueOf(Strings.CHECKOUT_PAYMENT_TITLE) + ' ' + 'Club 1',
                          contentPadding: EdgeInsets.zero,
                        ),
                        isEmpty: false,
                        child: DropdownButton<String>(
                          value: _allPaymentMethods[0],
                          onChanged: (String newValue) {
                            //setState(() {
                            //oldValue = newValue;
                            //});
                          },
                          items: _allPaymentMethods.map<DropdownMenuItem<String>>((String value) {
                            return DropdownMenuItem<String>(
                              value: value,
                              child: Text(value),
                            );
                          }).toList(),
                        ),
                      ),
                    )
                  ),

                  new Spacer(
                    flex: 1,
                  ),
                  new RaisedButton(
                      onPressed: () {
                        //Navigator.popUntil(context, ModalRoute.withName('/home'));
                        Navigator.pop(context);
                        Navigator.pop(context);
                      },
                      color: Theme.of(context).accentColor,
                      child: new Text(I18n.of(context).getValueOf(Strings.CHECKOUT_BUTTON_CONFIRM))
                  )
                ],
              )
            )
          ],
        )
      )
    );
  }
}

class CheckoutDataRow extends StatelessWidget {

  final String field;
  final String data;
  final Widget child;

  CheckoutDataRow({Key key, @required this.field, @required this.data, this.child}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final double _fontSize = 16;

    return new Column(
      children: <Widget>[
        new Container(
          height: 40,
          child: new Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              new Container(
                child: new Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 16.0),
                  child: new Text(field,
                    style: new TextStyle(
                      fontSize: _fontSize
                    )
                  )
                )
              ),
              new Expanded(
                child: new Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 16.0),
                  child: new Text(data,
                    style: new TextStyle(
                        fontSize: _fontSize
                    ),
                    textAlign: TextAlign.right,
                  )
                )
              )
            ],
          ),
        ),

        child != null ? child : new Container(),

        new Divider(height: 0, thickness: 1,)
      ],
    );
  }
}


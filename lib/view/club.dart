import 'package:flutter/material.dart';
import '../i18n.dart';
import '../color.dart';

class ClubView extends StatefulWidget {
  @override
  _ClubViewState createState() => _ClubViewState();
}

class _ClubViewState extends State<ClubView> {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(

      appBar: new PreferredSize(
        preferredSize: Size.fromHeight(130),
          child: new Stack(
            children: <Widget>[
              new Container(
                margin: const EdgeInsets.only(bottom: 28),
                child: new AppBar(
                  leading: new IconButton(
                    icon: new Icon(Icons.arrow_back),
                    onPressed: () {
                      Navigator.pop(context);
                    }
                  ),
                  title: new Text('Club name'),
                ),
              ),

              new Align(
                alignment: Alignment.bottomRight,
                  child: new Container(

                    child: new Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: <Widget>[

                        new Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 8.0),
                          child: new Tooltip(
                            message: I18n.of(context).getValueOf(Strings.CLUB_ACTION_CALL),
                            child: new FloatingActionButton(
                              heroTag: 'club_phone',
                              child: new Icon(Icons.phone),
                                onPressed: () {}
                            ),
                          )
                        ),
                        new Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 8.0),
                          child: new Tooltip(
                            message: I18n.of(context).getValueOf(Strings.CLUB_ACTION_EMAIL),
                            child: new FloatingActionButton(
                              heroTag: 'club_email',
                              child: new Icon(Icons.email),
                                onPressed: () {}
                            ),
                          )
                        ),
                        new Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 8.0),
                          child: new Tooltip(
                            message: I18n.of(context).getValueOf(Strings.CLUB_ACTION_FAVOURITE),
                            child: new FloatingActionButton(
                              heroTag: 'club_favourite',
                              child: new Icon(Icons.favorite),
                                onPressed: () {}
                            ),
                          )
                        ),

                        new Padding(
                          padding: const EdgeInsets.only(right: 8.0),
                        )

                      ],
                    )

                  )
              ),

            ],
            
          )
      ),

      body: new SafeArea(
        child: new SingleChildScrollView(
          child: new Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              new SizedBox(height: 10,),

              new Padding(
                padding: const EdgeInsets.symmetric(vertical: 16.0),
                child: new Text(I18n.of(context).getValueOf(Strings.CLUB_FACILITIES),
                  style: TextStyle(
                    fontSize: 18.0
                  ),
                ),
              ),

              new Card(
                elevation: 2.0,
                child: new Container(
                  height: 100,
                  child: new Column(
                    children: <Widget>[
                      new Flexible(
                        flex: 3,
                        child: new Stack(
                          children: <Widget>[
                            new Container(
                              decoration: new BoxDecoration(
                                borderRadius: BorderRadius.vertical(top: Radius.circular(4.0)),
                                image: new DecorationImage(
                                  image: new NetworkImage('https://cdn.pixabay.com/photo/2015/12/08/00/34/tennis-court-1081845_960_720.jpg'),
                                  fit: BoxFit.cover,
                                ),
                              ),
                            ),

                            new Align(
                                alignment: Alignment.bottomLeft,
                                child: new Padding(
                                  padding: const EdgeInsets.symmetric(vertical: 8.0, horizontal: 16.0),
                                  child: new Text('Tenis',
                                    style: TextStyle(
                                        fontSize: 20,
                                        fontWeight: FontWeight.w700,
                                        color: ThemeColor.primary(900)
                                    ),
                                  ),
                                )
                            ),

                          ],
                        )
                      ),
                      new Divider(height: 0,),
                      new Flexible(
                        flex: 1,
                        child: new Stack(
                          children: <Widget>[
                            new Align(
                              alignment: Alignment.bottomLeft,
                              child: new Padding(
                                padding: const EdgeInsets.symmetric(vertical: 4.0, horizontal: 16.0),
                                child: new Text('Pista rápida'),
                              ),
                            ),

                            new Align(
                              alignment: Alignment.bottomRight,
                              child: new Padding(
                                padding: const EdgeInsets.symmetric(vertical: 4.0, horizontal: 16.0),
                                child: new Text('3 pistas'),
                              ),
                            )
                          ],
                        )
                      )
                    ],
                  ),
                )
              ),

              new Card(
                elevation: 2.0,
                child: new Container(
                  height: 100,
                  child: new Column(
                    children: <Widget>[
                      new Flexible(
                        flex: 3,
                        child: new Stack(
                          children: <Widget>[
                            new Container(
                              decoration: new BoxDecoration(
                                borderRadius: BorderRadius.vertical(top: Radius.circular(4.0)),
                                image: new DecorationImage(
                                  image: new NetworkImage('https://cdn.pixabay.com/photo/2015/12/08/00/34/tennis-court-1081845_960_720.jpg'),
                                  fit: BoxFit.cover,
                                ),
                              ),
                            ),

                            new Align(
                              alignment: Alignment.bottomLeft,
                              child: new Padding(
                                padding: const EdgeInsets.symmetric(vertical: 8.0, horizontal: 16.0),
                                child: new Text('Tenis',
                                  style: TextStyle(
                                      fontSize: 20,
                                      fontWeight: FontWeight.w700,
                                      color: ThemeColor.primary(900)
                                  ),
                                ),
                              )
                            ),

                          ],
                        )
                      ),
                      new Divider(height: 0,),
                      new Flexible(
                        flex: 1,
                        child: new Stack(
                          children: <Widget>[
                            new Align(
                              alignment: Alignment.bottomLeft,
                              child: new Padding(
                                padding: const EdgeInsets.symmetric(vertical: 4.0, horizontal: 16.0),
                                child: new Text('Tierra batida'),
                              ),
                            ),

                            new Align(
                              alignment: Alignment.bottomRight,
                              child: new Padding(
                                padding: const EdgeInsets.symmetric(vertical: 4.0, horizontal: 16.0),
                                child: new Text('4 pistas'),
                              ),
                            )
                          ],
                        )
                      )
                    ],
                  ),
                )
              ),

              new Card(
                elevation: 2.0,
                child: new Container(
                  height: 100,
                  child: new Column(
                    children: <Widget>[
                      new Flexible(
                        flex: 3,
                        child: new Stack(
                          children: <Widget>[
                            new Container(
                              decoration: new BoxDecoration(
                                borderRadius: BorderRadius.vertical(top: Radius.circular(4.0)),
                                image: new DecorationImage(
                                  image: new NetworkImage('https://cdn.pixabay.com/photo/2015/12/08/00/34/tennis-court-1081845_960_720.jpg'),
                                  fit: BoxFit.cover,
                                ),
                              ),
                            ),

                            new Align(
                              alignment: Alignment.bottomLeft,
                              child: new Padding(
                                padding: const EdgeInsets.symmetric(vertical: 8.0, horizontal: 16.0),
                                child: new Text('Tenis',
                                  style: TextStyle(
                                    fontSize: 20,
                                    fontWeight: FontWeight.w700,
                                    color: ThemeColor.primary(900)
                                  ),
                                ),
                              )
                            ),

                          ],
                        )
                      ),
                      new Divider(height: 0,),
                      new Flexible(
                        flex: 1,
                        child: new Stack(
                          children: <Widget>[
                            new Align(
                              alignment: Alignment.bottomLeft,
                              child: new Padding(
                                padding: const EdgeInsets.symmetric(vertical: 4.0, horizontal: 16.0),
                                child: new Text('Hierba'),
                              ),
                            ),

                            new Align(
                              alignment: Alignment.bottomRight,
                              child: new Padding(
                                padding: const EdgeInsets.symmetric(vertical: 4.0, horizontal: 16.0),
                                child: new Text('1 pista'),
                              ),
                            )
                          ],
                        )
                      )
                    ],
                  ),
                )
              ),

            ],
          )
        )
      ),
    );
  }
}

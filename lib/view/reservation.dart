import 'package:flutter/material.dart';
import '../color.dart';
import '../i18n.dart';

import 'checkout.dart';

class ReservationView extends StatefulWidget {
  @override
  _ReservationViewState createState() => _ReservationViewState();
}

class _ReservationViewState extends State<ReservationView> {
  ScrollController _scrollController;
  bool lastStatus = true;

  _scrollListener() {
    if (isShrink != lastStatus) {
      setState(() {
        lastStatus = isShrink;
      });
    }
  }

  bool get isShrink {
    return _scrollController.hasClients &&
        _scrollController.offset > (90);
  }

  @override
  void initState() {
    _scrollController = ScrollController();
    _scrollController.addListener(_scrollListener);
    super.initState();
  }

  @override
  void dispose() {
    _scrollController.removeListener(_scrollListener);
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      body: new SafeArea(
        child: new CustomScrollView(
          controller: _scrollController,
          slivers: <Widget>[
            new SliverAppBar(
              actions: [
                new IconButton(
                  icon: new Icon(Icons.map),
                  onPressed: () {}
                )
              ],
              backgroundColor: ThemeColor.primary(500),
              floating: false,
              pinned: true,

              expandedHeight: 180,
              flexibleSpace: new FlexibleSpaceBar(
                title: new Text(I18n.of(context).getValueOf(Strings.DETAIL_TITLE),
                  style: TextStyle(
                    color: isShrink ? Colors.black : ThemeColor.primary(900),
                  ),
                ),
                background: new Hero(
                  tag: 'sportHero1',
                  child: new Image(
                    fit: BoxFit.cover,
                    image: new NetworkImage('https://cdn.pixabay.com/photo/2015/12/08/00/34/tennis-court-1081845_960_720.jpg'),
                  ),
                )
              )
            ),

            new SliverFillRemaining(
              fillOverscroll: true,
              child: new Padding(
                padding: const EdgeInsets.symmetric(horizontal: 8.0),
                child: new Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: <Widget>[
                    new Expanded(
                      child: new SingleChildScrollView(
                        child: new Column(
                          children: <Widget>[
                            new Container(height: 60,),
                            new CheckoutDataRow(
                              field: I18n.of(context).getValueOf(Strings.CHECKOUT_FIELD_SPORT),
                              data: 'Tenis (superficie)'
                            ),
                            new CheckoutDataRow(
                              field: I18n.of(context).getValueOf(Strings.CHECKOUT_FIELD_CLUB),
                              data: 'Club 1'
                            ),
                            new CheckoutDataRow(
                              field: I18n.of(context).getValueOf(Strings.CHECKOUT_FIELD_COURT),
                              data: 'Tenis 1',
                              child: new Padding(
                                padding: const EdgeInsets.only(bottom: 8.0),
                                child: new Column(
                                  children: <Widget>[
                                    new IconButton(
                                      icon: new Icon(Icons.lock),
                                      iconSize: 48.0,
                                      onPressed: () {}
                                    ),
                                    new Text(I18n.of(context).getValueOf(Strings.DETAIL_BUTTON_UNLOCK))
                                  ],
                                )
                              )
                            ),
                            new CheckoutDataRow(
                              field: I18n.of(context).getValueOf(Strings.CHECKOUT_FIELD_EXTRAS),
                              data: 'Ninguno'
                            ),
                            new CheckoutDataRow(
                              field: I18n.of(context).getValueOf(Strings.CHECKOUT_FIELD_DATE),
                              data: 'Domingo 24/11/19'
                            ),
                            new CheckoutDataRow(
                              field: I18n.of(context).getValueOf(Strings.CHECKOUT_FIELD_TIME),
                              data: '18:00 - 19:00'
                            ),
                            new CheckoutDataRow(
                              field: I18n.of(context).getValueOf(Strings.CHECKOUT_FIELD_PRICE),
                              data: '6,00€'
                            ),
                          ],
                        )
                      )
                    ),
                    new RaisedButton(
                      disabledColor: Colors.grey,
                      color: Theme.of(context).accentColor,
                      onPressed: () {
                        Navigator.pop(context);
                      },
                      child: new Text(I18n.of(context).getValueOf(Strings.DETAIL_BUTTON_CANCEL))
                    )
                  ],
                )
              )
            )

          ],
        )
      ),
    );
  }
}

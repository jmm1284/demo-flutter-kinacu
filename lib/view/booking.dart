import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:intl/intl.dart';

import 'package:flutter/cupertino.dart';
import 'package:sticky_headers/sticky_headers.dart';

import '../color.dart';
import '../i18n.dart';

import 'widgets/booking_datetime_picker.dart';
import 'widgets/booking_sport_picker.dart';
import 'widgets/booking_place_picker.dart';
import 'widgets/booking_duration_picker.dart';

class BookingState {
  DateTime dateTime;
  int sportId;
  String sportName;
  int duration;
  String durationName;
  int businessId;
  String businessName;

  BookingState(this.dateTime);

  static BookingState empty() {
    return new BookingState._internal();
  }

  BookingState._internal() {
    dateTime = null;
  }

  bool get validBookingState {
    return sportId != null && businessId != null && dateTime != null && duration != null;
  }
}


class _BookingRoot extends InheritedWidget {

  final BookingProviderState data;

  _BookingRoot({Key key, @required Widget child, @required this.data}) : super(key: key, child: child);

  @override
  bool updateShouldNotify(InheritedWidget oldWidget) => true;

}


class BookingProvider extends StatefulWidget {
  final Widget child;
  final BookingState bookingState;

  BookingProvider({this.bookingState, @required this.child});

  static BookingProviderState of(BuildContext context) {
    return (context.inheritFromWidgetOfExactType(_BookingRoot) as _BookingRoot).data;
  }

  @override
  BookingProviderState createState() => BookingProviderState(bookingState);
}

class BookingProviderState extends State<BookingProvider> {
  BookingState bookingState;

  BookingProviderState(this.bookingState);

  void updateSport(int id, String name) {
    setState(() {
      bookingState.sportId = id;
      bookingState.sportName = name;
    });
  }

  void updateDateTime(DateTime dateTime) {
    setState(() {
      bookingState.dateTime = dateTime;
    });
  }

  void updateDuration(int duration, String name) {
    setState(() {
      bookingState.duration = duration;
      bookingState.durationName = name;
    });
  }

  void updatePlace(int businessId, String businessName) {
    setState(() {
      bookingState.businessId = businessId;
      bookingState.businessName = businessName;
    });
  }


  @override
  Widget build(BuildContext context) {
    return _BookingRoot(
      data: this,
      child: widget.child,
    );
  }
}



class BookingView extends StatefulWidget {
  @override
  _BookingViewState createState() => _BookingViewState();
}

class _BookingViewState extends State<BookingView> {

  BookingState bookingState;
  bool _searchEnabled;


  @override
  void initState() {
    super.initState();
    _searchEnabled = false;
  }

  @override
  Widget build(BuildContext context) {

    final _provider = BookingProvider.of(context);
    double screenWidth = MediaQuery.of(context).size.width;
    var dateTimeFormatter = new DateFormat('dd-MM-yyyy HH:mm');

    return new Scaffold(
      appBar: new AppBar(
        title: new Text(I18n.of(context).getValueOf(Strings.BOOKING_TITLE)),
        elevation: 0,
      ),
      body: new SafeArea(
        child: new Column(
          children: <Widget>[
            new Container(
              decoration: new BoxDecoration(
                color: Theme.of(context).primaryColor,
              ),
              padding: const EdgeInsets.only(top: 4.0, bottom: 16.0),
              child: new Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[

                  new Expanded(
                    child: new BookingStep(
                      icon: new Icon(FontAwesomeIcons.tableTennis),
                      text: _provider.bookingState.sportName == null
                          ? I18n.of(context).getValueOf(Strings.BOOKING_STEP_SPORT)
                          : _provider.bookingState.sportName,
                      status: _provider.bookingState.sportName != null,
                      function: () {
                        showModalBottomSheet(
                          isScrollControlled: false,
                          context: context,
                          builder: (BuildContext context) {
                            return new SportPicker(
                              state: _provider,
                            );
                          }
                        );
                      }
                    )
                  ),

                  new Expanded(
                    child: new BookingStep(
                      icon: new Icon(FontAwesomeIcons.mapMarkedAlt),
                      text: _provider.bookingState.businessId == null
                          ? I18n.of(context).getValueOf(Strings.BOOKING_STEP_WHERE)
                          : _provider.bookingState.businessName,
                      status: _provider.bookingState.businessId != null,
                      function: () {
                        showModalBottomSheet(
                          context: context,
                          builder: (BuildContext builder) {
                            return new PlacePicker(
                              state: _provider,
                            );
                          }
                        );
                      },
                    )
                  ),

                  new Expanded(
                    child: new BookingStep(
                      icon: new Icon(FontAwesomeIcons.calendarAlt),
                      text: _provider.bookingState.dateTime == null
                          ? I18n.of(context).getValueOf(Strings.BOOKING_STEP_WHEN)
                          : dateTimeFormatter.format(_provider.bookingState.dateTime),
                      status: _provider.bookingState.dateTime != null,
                      function: () {
                        showModalBottomSheet(
                          context: context,
                          builder: (BuildContext builder) {
                            return new DateTimePicker(
                              state: _provider,
                            );
                          }
                        );
                      },
                    )
                  ),

                  new Expanded(
                    child: new BookingStep(
                      icon: new Icon(FontAwesomeIcons.stopwatch),
                      text: _provider.bookingState.duration == null
                          ? I18n.of(context).getValueOf(Strings.BOOKING_STEP_HOW_LONG)
                          : _provider.bookingState.durationName,
                      status: _provider.bookingState.duration != null,
                      function: () {
                        showModalBottomSheet(
                          context: context,
                          builder: (BuildContext builder) {
                            return new DurationPicker(
                              state: _provider,
                            );
                          }
                        );
                      },
                    )
                  ),


                ],
              ),
            ),

            new Expanded(
              child: new Stack(
                children: <Widget>[
                  new Align(
                    alignment: Alignment.topCenter,
                    child: new Container(
                      padding: const EdgeInsets.only(top: 18),
                      child: new NestedScrollView(
                        headerSliverBuilder: (BuildContext context, bool scrolledBox) {
                          return <Widget>[];
                        },
                        body: _searchEnabled
                        ?
                        new CustomScrollView(
                          slivers: <Widget>[
                            new SliverList(
                              delegate: new SliverChildBuilderDelegate((BuildContext context, int index) {
                                return new Container(
                                  padding: const EdgeInsets.only(top: 0),
                                  child: new StickyHeader(
                                    header: new Container(
                                      decoration: new BoxDecoration(
                                          //color: Color.fromRGBO(255, 255, 255, 1)
                                        color: ThemeColor.primary(50)
                                      ),
                                      padding: const EdgeInsets.all(0),
                                      height: 80,
                                      child: new Row(
                                        mainAxisAlignment: MainAxisAlignment.end,
                                        crossAxisAlignment: CrossAxisAlignment.end,
                                        children: <Widget>[
                                          new Expanded(
                                            child: new Padding(
                                              padding: const EdgeInsets.only(left: 24, bottom: 12),
                                              child: new Text('Nombre del club',
                                                style: TextStyle(
                                                    fontSize: 20,
                                                    fontWeight: FontWeight.bold
                                                ),
                                              ),
                                            )
                                          ),

                                          new IconButton(
                                            icon: new Icon(Icons.info_outline),
                                            onPressed: (){
                                              Navigator.pushNamed(context, '/club');
                                            }
                                          ),
                                        ],
                                      ),
                                    ),
                                    content: new Container(
                                      child: new Column(
                                        mainAxisAlignment: MainAxisAlignment.start,
                                        children: <Widget>[


                                          new AvailabilityRow(),
                                          new Padding(
                                            padding: const EdgeInsets.only(right: 16, left: 16),
                                            child: new Divider(
                                              height: 0,
                                              thickness: 1,
                                            ),
                                          ),
                                          new AvailabilityRow(),
                                          new Padding(
                                            padding: const EdgeInsets.only(right: 16, left: 16),
                                            child: new Divider(
                                              height: 0,
                                              thickness: 1,
                                            ),
                                          ),
                                          new AvailabilityRow()
                                        ],
                                      ),
                                    )
                                  ),
                                );
                              })
                            )
                          ],
                        )
                            :
                        new Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.stretch,
                          children: <Widget>[
                            new Container(
                              height: 96,
                              width: 96,
                              child: new Image(image: new AssetImage('assets/images/no_results.png')),
                            ),
                            new Text(I18n.of(context).getValueOf(Strings.BOOKING_PLACEHOLDER_1),
                              textAlign: TextAlign.center,
                              style: TextStyle(fontSize: 16),
                            ),
                            new Text(I18n.of(context).getValueOf(Strings.BOOKING_PLACEHOLDER_2),
                              textAlign: TextAlign.center,
                              style: TextStyle(fontSize: 16),
                            )

                          ]
                        ),
                      )
                    )
                  ),

                  new Container(
                    height: 30,
                    decoration: new BoxDecoration(
                      color: Theme.of(context).primaryColor,
                      borderRadius: new BorderRadius.only(
                          bottomLeft: Radius.elliptical(screenWidth * 4, 100),
                          bottomRight: Radius.elliptical(screenWidth * 4, 100)
                      )
                    ),
                  ),

                  new Align(
                    alignment: Alignment.topCenter,
                    child: new Container(
                      width: screenWidth * 0.8,
                      child: new RaisedButton(
                        disabledColor: Colors.grey,
                        color: Theme.of(context).accentColor,
                        child: new Text(I18n.of(context).getValueOf(Strings.BOOKING_BUTTON_SEARCH)),
                        onPressed: _provider.bookingState.validBookingState
                          ? () {
                              setState(() {
                                _searchEnabled = true;
                              });
                            }
                          : null
                      )
                    )
                  ),





                ],
              )
            )
          ],
        )

      )
    );
  }
}

class BookingStep extends StatefulWidget {
  final Icon icon;
  final String text;
  final bool status;
  final VoidCallback function;

  const BookingStep({
    Key key,
    @required this.icon,
    @required this.text,
    @required this.status,
    this.function
  }): super(key: key);

  @override
  _BookingStepState createState() => _BookingStepState();
}

class _BookingStepState extends State<BookingStep> {
  @override
  Widget build(BuildContext context) {

    double _iconSize = 56.0;

    return new Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: <Widget>[

        new Padding(
          padding: const EdgeInsets.all(8.0),
          child: new Stack(
            children: <Widget>[

              new Align(
                child: new Container(
                  width: _iconSize,
                  height: _iconSize,
                  decoration: new BoxDecoration(
                      color: Color.fromRGBO(220, 220, 220, 0.5),
                      shape: BoxShape.circle
                  ),
                  child: new IconButton(
                      iconSize: 24.0,
                      icon: widget.icon,
                      onPressed: widget.function,
                    color: Colors.black,
                  ),
                ),
              ),

              new Align(
                alignment: Alignment(0.5, -1.0),
                child: new Container(
                  width: 16.0,
                  height: 16.0,
                  decoration: new BoxDecoration(
                      color: widget.status ? Colors.lightGreenAccent : Colors.red,
                      shape: BoxShape.circle
                  ),
                  child: new Icon(
                      widget.status ? FontAwesomeIcons.check : FontAwesomeIcons.times,
                    size: 12.0,
                  ),
                ),
              )


            ],
          ),
        ),

        new Padding(
          padding: const EdgeInsets.symmetric(horizontal: 2.0),
          child: new Container(
            padding: const EdgeInsets.symmetric(vertical: 4.0, horizontal: 8.0),
            decoration: new BoxDecoration(

              shape: BoxShape.rectangle,
              borderRadius: BorderRadius.all(Radius.circular(32.0))
            ),
            child: new Tooltip(
              message: widget.text,
              child: new Text(widget.text,
                textAlign: TextAlign.center,
                overflow: TextOverflow.ellipsis,
                maxLines: 2,
              )
            )
          ),
        )

      ],
    );
  }
}

class AvailabilityRow extends StatefulWidget {
  @override
  _AvailabilityRowState createState() => _AvailabilityRowState();
}

class _AvailabilityRowState extends State<AvailabilityRow> {
  @override
  Widget build(BuildContext context) {
    return new InkWell(
      onTap: (){
        Navigator.pushNamed(context, '/checkout');
      },
      child: new Container(
        margin: const EdgeInsets.symmetric(vertical: 4.0),
        height: 64.0,
        child: new Row(
          children: <Widget>[
            new Padding(
              padding: const EdgeInsets.only(left: 8.0, right: 16.0),
              child: new Container(
                width: 56.0,
                height: 56.0,
                decoration: new BoxDecoration(
                  shape: BoxShape.circle,
                  image: new DecorationImage(
                      fit: BoxFit.cover,
                      image: new NetworkImage('https://cdn.pixabay.com/photo/2015/12/08/00/34/tennis-court-1081845_960_720.jpg')
                  )
                )
              ),
            ),

            new Expanded(
              child: new Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  new Row(
                    children: <Widget>[
                      new Flexible(
                        child: new Text('Pista 1',
                          overflow: TextOverflow.ellipsis,
                          maxLines: 1,
                          style: new TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 18.0
                          ),
                        ),
                      ),
                      new SizedBox(
                        width: 12,
                        child: new Text('·', textAlign: TextAlign.center,),
                      ),
                      new Expanded(
                          child: new Text('(superficie)',
                            overflow: TextOverflow.ellipsis,
                            maxLines: 1,
                          ),
                      ),
                    ],
                  ),
                  new Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      new Text('18:00 - 19:00',
                        overflow: TextOverflow.ellipsis,
                        maxLines: 1,
                        style: new TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 20.0
                        ),
                      ),
                      new Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 8.0),
                        child: new Icon(FontAwesomeIcons.solidLightbulb,
                          size: 16.0,
                          color: Theme.of(context).primaryColorDark,
                        )
                      )
                    ],
                  )
                ],
              )

            ),

            new Padding(
              padding: const EdgeInsets.only(left: 16.0, right: 8.0),
              child: new Text('14,00€',
                style: new TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 24.0,
                  color: Theme.of(context).primaryColorDark
                ),
              ),
            )

          ],
        ),
      )
    );
  }
}

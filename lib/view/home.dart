import 'package:flutter/material.dart';
import '../color.dart';
import '../i18n.dart';

import 'widgets/home_drawer.dart';
import 'reservation.dart';

class HomeState {

  HomeState();

  static HomeState empty() {
    return new HomeState._internal();
  }

  HomeState._internal();
}

class _HomeRoot extends InheritedWidget {

  final HomeProviderState data;

  _HomeRoot({Key key, @required Widget child, @required this.data}) : super(key: key, child: child);

  @override
  bool updateShouldNotify(InheritedWidget oldWidget) => true;

}

class HomeProvider extends StatefulWidget {
  final Widget child;
  final HomeState homeState;

  HomeProvider({this.homeState, @required this.child});

  static HomeProviderState of(BuildContext context) {
    return (context.inheritFromWidgetOfExactType(_HomeRoot) as _HomeRoot).data;
  }

  @override
  HomeProviderState createState() => HomeProviderState(homeState);
}

class HomeProviderState extends State<HomeProvider> {
  HomeState homeState;

  HomeProviderState(this.homeState);


  @override
  Widget build(BuildContext context) {
    return _HomeRoot(
      data: this,
      child: widget.child,
    );
  }
}


class HomeView extends StatefulWidget {
  @override
  _HomeViewState createState() => _HomeViewState();
}

class _HomeViewState extends State<HomeView> {

  HomeState homeState;

  @override
  void initState() {
    super.initState();
  }

  List<Widget> reservations = [

    new ItemFeed(
      bookingId: 1,
      businessName: null,
      sportId: null,
      sportName: null,
      resourceTypeName: null,
      date: null,
      time: null,
      price: null,
      lighting: true,
      openCode: true,
    ),

    new ItemFeed(
      bookingId: 2,
      businessName: null,
      sportId: null,
      sportName: null,
      resourceTypeName: null,
      date: null,
      time: null,
      price: null,
      lighting: true,
      openCode: false,
    ),

    new ItemFeed(
      bookingId: 3,
      businessName: null,
      sportId: null,
      sportName: null,
      resourceTypeName: null,
      date: null,
      time: null,
      price: null,
      lighting: false,
      openCode: true,
    ),

    new ItemFeed(
      bookingId: 4,
      businessName: null,
      sportId: null,
      sportName: null,
      resourceTypeName: null,
      date: null,
      time: null,
      price: null,
      lighting: false,
      openCode: false,
    ),

  ];
  

  @override
  Widget build(BuildContext context) {
    double screenWidth = MediaQuery.of(context).size.width;

    return new Scaffold(
      bottomNavigationBar: new BottomAppBar(
        color: Theme.of(context).primaryColor,
        shape: CircularNotchedRectangle(),
        notchMargin: 4,
        child: new Container(
          height: 55,
        ),
      ),

      floatingActionButtonLocation: FloatingActionButtonLocation.endDocked,
      floatingActionButton: FloatingActionButton(
        child: new Icon(Icons.add),
        onPressed: () {
          Navigator.pushNamed(context, '/booking');
        }
      ),
      drawer: new HomeDrawer(),
      appBar: new AppBar(
        backgroundColor: Theme.of(context).primaryColor,
        elevation: 0.0,
        title: new Text(I18n.of(context).getValueOf(Strings.HOME_TITLE)),
      ),
      body: new SafeArea(
        child: new Container(
          child: new Stack(
            children: <Widget>[
              reservations.isNotEmpty
                ? new ListView(
                  children: <Widget>[
                    new Container(height: 18),
                    ...reservations,
                    new Container(height: 28)
                  ]
                )
                : new Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: <Widget>[
                    new Container(
                      height: 96,
                      width: 96,
                      child: new Image(image: new AssetImage('assets/images/no_match.png')),
                    ),
                    new Text(I18n.of(context).getValueOf(Strings.HOME_EMPTY_PLACEHOLDER_1),
                      textAlign: TextAlign.center,
                      style: TextStyle(fontSize: 16),
                    ),
                    new Text(I18n.of(context).getValueOf(Strings.HOME_EMPTY_PLACEHOLDER_2),
                      textAlign: TextAlign.center,
                      style: TextStyle(fontSize: 16),
                    )

                  ]
                ),


              new Container(
                height: 16,
                decoration: new BoxDecoration(
                    color: Theme.of(context).primaryColor,
                    borderRadius: new BorderRadius.only(
                        bottomLeft: Radius.elliptical(screenWidth * 4, 100),
                        bottomRight: Radius.elliptical(screenWidth * 4, 100)
                    )
                ),
              ),

            ],
          )
        )
      )
    );
  }
}

class ItemFeed extends StatelessWidget {
  final int bookingId;
  final String businessName;
  final int sportId;
  final String sportName;
  final String resourceTypeName;
  final String date;
  final String time;
  final String price;
  final bool lighting;
  final bool openCode;

  ItemFeed({
    Key key,
    @required this.bookingId,
    @required this.businessName,
    @required this.sportId,
    @required this.sportName,
    @required this.resourceTypeName,
    @required this.date,
    @required this.time,
    @required this.price,
    this.lighting = null ?? false,
    this.openCode = null ?? false
  });

  @override
  Widget build(BuildContext context) {
    int _bookingId = bookingId;
    return new Card(
      elevation: 2.0,
      child: new Container(
        height: 160,
        child: new InkWell(
          splashColor: Colors.blue.withAlpha(30),
          onTap: () {
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => ReservationView())
            );
          },
          child: new Column(
            children: <Widget>[
              new Flexible(
                flex: 3,
                  child: new Stack(
                    children: <Widget>[
                      new Hero(
                        tag: 'sportHero'+_bookingId.toString(),
                        child: new Container(
                          decoration: new BoxDecoration(
                            borderRadius: BorderRadius.vertical(top: Radius.circular(4.0)),
                            image: new DecorationImage(
                              image: new NetworkImage('https://cdn.pixabay.com/photo/2015/12/08/00/34/tennis-court-1081845_960_720.jpg'),
                              fit: BoxFit.cover,
                            ),
                          ),
                        ),
                      ),
                      
                      new Align(
                        alignment: Alignment.topRight,
                        child: new Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: <Widget>[
                            if (openCode) new Tooltip(
                                message: I18n.of(context).getValueOf(Strings.HOME_TOOLTIP_ICON_CODE),
                                child: new IconButton(
                                  icon: new Icon(Icons.lock),
                                  color: ThemeColor.accent(300),
                                  onPressed: () {}
                                ),
                              ),
                            if (lighting)
                              new Tooltip(
                                message: I18n.of(context).getValueOf(Strings.HOME_TOOLTIP_ICON_LIGHT),
                                child: new IconButton(
                                  icon: new Icon(Icons.highlight),
                                    color: ThemeColor.accent(300),
                                  onPressed: () {}
                                )
                              ),
                          ],
                        ),
                      ),

                      new Align(
                        alignment: Alignment.bottomLeft,
                        child: new Padding(
                          padding: const EdgeInsets.symmetric(vertical: 8.0, horizontal: 16.0),
                          child: new Text('Sábado \n30/11/2019',
                            style: TextStyle(
                              fontSize: 18,
                              fontWeight: FontWeight.w700,
                              color: ThemeColor.primary(900)
                            ),
                          ),
                        )
                      ),

                      new Align(
                        alignment: Alignment.bottomRight,
                        child: new Padding(
                          padding: const EdgeInsets.symmetric(vertical: 8.0, horizontal: 16.0),
                          child: new Text('18:00 - 19:00',
                            style: TextStyle(
                              fontSize: 22,
                              fontWeight: FontWeight.w700,
                              color: ThemeColor.primary(900)
                            ),
                          ),
                        )
                      ),



                    ],
                  )
              ),
              new Divider(height: 0,),
              new Flexible(
                flex: 2,
                  child: new Stack(
                    children: <Widget>[

                      new Align(
                        alignment: Alignment.topLeft,
                        child: new Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            new Padding(
                              padding: const EdgeInsets.only(top: 8.0, left: 16.0, right: 16.0),
                              child: new Text('Tennis 1 (pista rápida)',

                              ),
                            ),
                            new Padding(
                              padding: const EdgeInsets.symmetric(vertical: 4.0, horizontal: 16.0),
                              child: new Text('Kinacu club'),
                            ),
                          ],
                        )
                      ),


                    ],
                  )
              )
            ],
          ),
        ),
      )
    );

  }
}


import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'i18n.dart';

import 'color.dart';

import 'view/login.dart';
import 'view/booking.dart';
import 'view/checkout.dart';
import 'view/home.dart';
import 'view/reservation.dart';
import 'view/register.dart';
import 'view/club.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      onGenerateRoute: (RouteSettings settings) {
        switch (settings.name) {
          case '/':
            return MaterialPageRoute(builder: (context) => LoginView());
          case '/register':
            return MaterialPageRoute(builder: (context) => RegisterView());
          case '/home':
            return MaterialPageRoute(builder: (context) => HomeView());
          case '/booking':
            return MaterialPageRoute(builder: (context) => new BookingProvider(
                bookingState: BookingState.empty(),
                child: new BookingView()
            ));
          case '/checkout':
            return MaterialPageRoute(builder: (context) => CheckoutView());
          case '/reservation':
            return MaterialPageRoute(builder: (context) => ReservationView());
          case '/club':
            return MaterialPageRoute(builder: (context) => ClubView());
          default:
            return MaterialPageRoute(builder: (context) => new Container()); //NOT FOUND
        }
      },
      localizationsDelegates: [
        I18nDelegate(),
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
        GlobalCupertinoLocalizations.delegate,
      ],
      supportedLocales: [
        const Locale('es', 'ES'),
        const Locale('en')
      ],
      title: 'Kinacu Demo',
      theme: ThemeData(
        primaryColor: ThemeColor.primary(500),
        primaryColorLight: ThemeColor.primary(200),
        primaryColorDark: Color(0xFF009A99),
        accentColor: ThemeColor.accent(500),
        fontFamily: 'Cabin'
      ),
      home: new LoginView()
          //child: new BookingView()
          //child: new CheckoutView()
          //child: new HomeView(),
          //child: new ReservationView(),
          //child: new RegisterView(),
          //child: new ClubView(),

            /*
            new BookingProvider(
              bookingState: BookingState.empty(),
              child: new BookingView()
            )
             */

    );
  }
}



